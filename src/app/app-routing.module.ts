import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ActiviteitenComponent } from './pages/activiteiten/activiteiten.component';
import { ProsenioresComponent } from './pages/proseniores/proseniores.component';
import { PraesidiumComponent } from './pages/praesidium/praesidium.component';
import { GeschiedenisComponent } from './pages/geschiedenis/geschiedenis.component';
import { SchildComponent } from './pages/schild/schild.component';
import { ClubliedComponent } from './pages/clublied/clublied.component';
import { StatutenComponent } from './pages/statuten/statuten.component';
import { HistorischeContextComponent } from './pages/geschiedenis/historische-context/historische-context.component';
import { OprichtersComponent } from './pages/geschiedenis/oprichters/oprichters.component';
import { OprichtingComponent } from './pages/geschiedenis/oprichting/oprichting.component';
import { ReguleComponent } from './pages/geschiedenis/regule/regule.component';
import { WeetjesComponent } from './pages/geschiedenis/weetjes/weetjes.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'activiteiten', component: ActiviteitenComponent},
  {path: 'proseniores', component: ProsenioresComponent},
  {path: 'praesidium', component: PraesidiumComponent},
  {path: 'geschiedenis', component: GeschiedenisComponent},
  {path: 'geschiedenis/historische-context', component: HistorischeContextComponent},
  {path: 'geschiedenis/oprichters', component: OprichtersComponent},
  {path: 'geschiedenis/oprichting', component: OprichtingComponent},
  {path: 'geschiedenis/regule', component: ReguleComponent},
  {path: 'geschiedenis/weetjes', component: WeetjesComponent},
  {path: 'schild', component: SchildComponent},
  {path: 'statuten', component: StatutenComponent},
  {path: 'clublied', component: ClubliedComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
