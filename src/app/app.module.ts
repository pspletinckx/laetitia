import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { ActiviteitenComponent } from './pages/activiteiten/activiteiten.component';
import { ProsenioresComponent } from './pages/proseniores/proseniores.component';
import { PraesidiumComponent } from './pages/praesidium/praesidium.component';
import { GeschiedenisComponent } from './pages/geschiedenis/geschiedenis.component';
import { SchildComponent } from './pages/schild/schild.component';
import { StatutenComponent } from './pages/statuten/statuten.component';
import { ClubliedComponent } from './pages/clublied/clublied.component';
import { HistorischeContextComponent } from './pages/geschiedenis/historische-context/historische-context.component';
import { OprichtersComponent } from './pages/geschiedenis/oprichters/oprichters.component';
import { OprichtingComponent } from './pages/geschiedenis/oprichting/oprichting.component';
import { WeetjesComponent } from './pages/geschiedenis/weetjes/weetjes.component';
import { ReguleComponent } from './pages/geschiedenis/regule/regule.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ActiviteitenComponent,
    ProsenioresComponent,
    PraesidiumComponent,
    GeschiedenisComponent,
    SchildComponent,
    StatutenComponent,
    ClubliedComponent,
    HistorischeContextComponent,
    OprichtersComponent,
    OprichtingComponent,
    WeetjesComponent,
    ReguleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
