import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClubliedComponent } from './clublied.component';

describe('ClubliedComponent', () => {
  let component: ClubliedComponent;
  let fixture: ComponentFixture<ClubliedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClubliedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClubliedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
