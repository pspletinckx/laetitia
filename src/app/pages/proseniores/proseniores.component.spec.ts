import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProsenioresComponent } from './proseniores.component';

describe('ProsenioresComponent', () => {
  let component: ProsenioresComponent;
  let fixture: ComponentFixture<ProsenioresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProsenioresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProsenioresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
