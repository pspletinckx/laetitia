import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorischeContextComponent } from './historische-context.component';

describe('HistorischeContextComponent', () => {
  let component: HistorischeContextComponent;
  let fixture: ComponentFixture<HistorischeContextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorischeContextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorischeContextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
