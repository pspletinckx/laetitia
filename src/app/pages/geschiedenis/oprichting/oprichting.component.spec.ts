import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OprichtingComponent } from './oprichting.component';

describe('OprichtingComponent', () => {
  let component: OprichtingComponent;
  let fixture: ComponentFixture<OprichtingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OprichtingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OprichtingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
