import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OprichtersComponent } from './oprichters.component';

describe('OprichtersComponent', () => {
  let component: OprichtersComponent;
  let fixture: ComponentFixture<OprichtersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OprichtersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OprichtersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
