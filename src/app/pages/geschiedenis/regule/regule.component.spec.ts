import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReguleComponent } from './regule.component';

describe('ReguleComponent', () => {
  let component: ReguleComponent;
  let fixture: ComponentFixture<ReguleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReguleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReguleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
