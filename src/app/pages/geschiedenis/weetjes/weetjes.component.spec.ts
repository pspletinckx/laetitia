import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeetjesComponent } from './weetjes.component';

describe('WeetjesComponent', () => {
  let component: WeetjesComponent;
  let fixture: ComponentFixture<WeetjesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeetjesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeetjesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
