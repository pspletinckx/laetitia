FROM nginx:alpine
COPY dist/laetitia /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf